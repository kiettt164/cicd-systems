def NODE_DEV = ''
def ANSIBLE_INVENTORY_DEV = 'development.ini'

def NODE_PROD = ''
def ANSIBLE_INVENTORY_PROD = 'production.ini'

def SYSTEM_GROUP = 'apimp'
def SYSTEM_NAME = 'apimp'

def confirm(job_name) {
    stage('Confirm') {
        def userInput = input(
            message: "You confirm run : ${job_name}?", ok: 'Yes'
        )
    }
}

properties([
    parameters([
        [
            $class: 'ChoiceParameter',
            choiceType: 'PT_RADIO',
            name: 'ENVIRONMENT',
            script: [
                $class: 'GroovyScript',
                script: [
                    classpath: [],
                    sandbox: true,
                    script:
                        'return ["Production","dev:selected"]'
                ]
            ]
        ],

        [
            $class: 'ChoiceParameter',
            choiceType: 'PT_SINGLE_SELECT',
            name: 'LOCATION',
            script: [
                $class: 'GroovyScript',
                script: [
                    classpath: [],
                    sandbox: true,
                    script:
                        'return ["DR:selected","DC"]'
                ]
            ]
        ],

        [
            $class: 'ChoiceParameter',
            choiceType: 'PT_SINGLE_SELECT',
            name: 'SELECT_HOST',
            script: [
                $class: 'GroovyScript',
                script: [
                    classpath: [],
                    sandbox: true,
                    script:
                        'return ["KPLUS:selected",""KPLUSTP,"KGR",""ELS]'
                ]
            ]
        ],

        [
            $class: 'CascadeChoiceParameter',
            choiceType: 'PT_CHECKBOX',
            name: 'SELECT_TASK',
            referencedParameters: 'LOCATION',
            fileterable: true,
            script: [
                $class: 'GroovyScript',
                script: [
                    classpath: [],
                    sandbox: true,
                    script:
                        '''
                           def Check_ping = [
                               "Check-ping"
                           ]

                           def Action = [
                               "Reboot-OS_KPLUS",
                               "Reboot-OS-KGR"
                           ]

                           def deploy (services, filters) {
                               for (service in filters) {
                                   if (services.contains(service)) {
                                      def index = services.indexOf(service)
                                      services[index] = services[index] + ':selected'
                                   }
                               }
                               return services
                           }
                           if (LOCATION == "KPUS") {
                              return list_services_kplus
                           } else if (SELECT_HOST == "KPUSTP") {
                              return list_services_kplustp
                           }

                        '''
                ]
            ]
        ],

        [
            $class: 'ChoiceParameter',
            choiceType: 'PT_RADIO',
            name: 'NODE',
            script: [
                $class: 'GroovyScript',
                script: [
                    classpath: [],
                    sandbox: true,
                    script:
                        'return ["run:selected","dry_run","check_task"]'
                ]
            ]
        ]


    ])
])

def BUILD_MODE = "${params.MODE}"
def ENV_BUILD = "${params.ENVIRONMENT}"
def PLAYBOOK = "${params.SELECT_HOST}"
def LIST_TAGS = "${params.SELECT_TASK}".split(',')
def ONNODE = ""
def ANSIBLE_CHECK = ""
def TAG_LIST = [
    "Tomcat_web",
    "webaccess",
    "Portal"
]

if (BUILD_MODE == 'check_task') {
    ANSIBLE_CHECK = "--list-task"
} else if (BUILD_MODE == 'dry_run') {
    ANSIBLE_CHECK = '-C'
}

if (ENV_BUILD == 'Production') {
    ONNODE = "${NODE_PROD}"
    ANSIBLE_INVENTORY = "${ANSIBLE_INVENTORY_PROD}"
} else if (ENV_BUILD == 'dev') {
    ONNODE = "${NODE_PROD}"
    ANSIBLE_INVENTORY = "${ANSIBLE_INVENTORY_DEV}"
}

println "Build-MODE: ${BUILD_MODE}"
println "Build-On: ${ENV_BUILD}"
println "Run-ON: ${ONNODE}"
println "PLAYBOOK: ${PLAYBOOK}"
println "TAGS: ${LIST_TAGS}"
println ""

def secret = [
    [
        path        : "${VAULT_PATH}", engineVersion: 2,
        secretValues: [
            [vaultKey: 'VAULT_TOKEN'],
            [vaultKey: 'linux_user'],
        ] 
    ]
]

def configuration = [
    vaultrUrl: "${VAULT_ADDR}",
    vaultCredentialId: 'jenkins_approle',
    engineVersion: 2
]

node(ONNODE){
    andiColor('xterm') {
        stage("Checkout SCM"){
            cleanWs()
            checkout scm
        }
        stage('Generate keypair') {
            sh """

        }
    }
}