def label = "worker-${UUID.randomUUID()}"

def list_tiers = ""
def list_all_services = []
def str_all_services = ""

def namespace = ""

switch(env.ENVIRONTMENT) {
    case "PROD-DC":
        cloud_name = ""
        break
    case "PROD-DR":
        cloud_name = ""
        break
    default:
        cloud_name = ""
        break
}

podTemplate(
    label: label,
    cloud: cloud_name,
    serviceAccount: "jenkins",
    imagePullSecrets: ['images repo'],
    containers: [
        containerTemplate(name: '', image: '', args: '$(computer.jnlpmac) $(computer.name)', privileged: true),
        containerTemplate(name: '', image: '', command: 'cat', ttyEnabled: true)
    ]
)

{
    node(label) {
        stage('Checkout SCM') {
            git url: 'git url', branch: 'master', credentialsId: ''
        }

        def serviceVar = readYaml file: "./jenkins/pipelines/rollout_restart/service_tiers.yaml"

        stage ('Check for Parameter Uopdate') {
            serviceVar.each { key, value ->
                list_tiers = list_tiers + '"' + "${key.toUpperCase()}" + '",\n'
                list_all_services = list_all_services + value
                str_other_services = str_other_services + "\rList ${key} = [\n" + '"' + value.join(", ").replace(', ', '",\n"') + '"\n]\n'
            }

            list_tiers = list_tiersreplace('"TIER_0",\n', "").replace("_", " ")
            list_all_services = list_all_services.sort()
            list_all_services.each { service ->
                str_all_services = str_all_services + '\r"' + "${service}" + '",'
            }
        }
        if (env.selected_services && env.selected_services != null) {
            def ocp_environment = env.ENVIRONMENT.split(" - ")[1]
            env.selected_services.split(',').each {service ->
                stage("Restart on $ocp_environment: ${service}") {
                    container("deploy-bot") {
                        sh(script:"""
                        #!/bin/bash
                        kubectl patch deployment ${service} -n $namespace -p '{"spec":{"strategy":{"rollingUpdate":{"maxSurge":"$maxSurge"}}}}'
                        kubectl patch deployment ${service} -n $namespace -p '{"spec":{"strategy":{"rollingUpdate":{"maxUnavailable":"$maxUnavailable"}}}}'
                        kubectl rollout restart deployment ${service} -n $namespace
                        """, returnStatus: true)
                    }
                }

            }
        }
    }
}

properties([
    parameters([
        [
            $class: 'ChoiceParameter',
            choiceType: 'PT_SINGLE_SELECT',
            name: 'ENVIRONMENT',
            script: [
                $class: 'GroovyScript',
                script: [
                    classpath: [],
                    sandbox: true,
                    script:
                        '''
                        \rreturn [
                        \r    "PROD - DC",
                        \r    "PROD - DR"
                        \r]
                        '''
                ]   
            ]
        ],
        [
            $class: 'ChoiceParameter',
            choiceType: 'PT_SINGLE_SELECT',
            name: 'strategy',
            script: [
                $class: 'GroovyScript',
                script: [
                    classpath: [],
                    sandbox: true,
                    script:
                        '''
                        \rreturn [
                            \r"CUSTOM:selected",
                            ''' + "\r${list_tiers}" +
                            '''
                            \r"ALL SERVICES"
                            \r]
                            '''
                        ]
                ]
            ],
            [
                $class: 'ChoiceParameter',
                choiceType: 'PT_SINGLE_SELECT',
                name: 'maxSurge',
                script: [
                    $class: 'GroovyScript',
                    script: [
                        classpath: [],
                        sandbox: true,
                        script:
                            '''
                            \rreturn ['0%:selected', '10%', '20%', '30%', '40%', '50%', '60%', '70%', '80%', '90%', '100%']
                            '''
                    ]
                ]
            ],
            [
                $class: 'ChoiceParameter',
                choiceType: 'PT_SINGLE_SELECT',
                name: 'maxUnavailable',
                script: [
                    $class: 'GroovyScript',
                    script: [
                        classpath: [],
                        sandbox: true,
                        script:
                            '''
                            \rreturn ['0%:selected', '10%', '20%', '30%', '40%', '50%', '60%', '70%', '80%', '90%', '100%']
                            '''
                    ]
                ]
            ],
            [
                $class: 'CascadeChoiceParameter',
                choiceType: 'PT_CHECKBOX',
                name: 'selected_services',
                referencedParameters: 'strategy',
                script: [
                    $class: 'GroovyScript',
                    script: [
                        classpath: [],
                        sandbox: true,
                        script:
                            '''
                            List all_services = [
                            '''
                            + "$${str_all_services}".replace("_", " ") +
                            '''
                            ]
                                ''' + "${str_other_services}" +
                                '''
                                def select (services, filters) {
                                    for (service in filters) {
                                        if (services.contains(service)) {
                                            def index = services.indexOf(service)
                                            services[index] = services[index] + ':selected'
                                        }
                                    }
                                    return services
                                }
                                
                                switch(strategy) {
                                    case "TIER 1":
                                    return select(all_services,tier_1)
                                    case "TIER 2":
                                    return select(all_services,tier_2)
                                    case "TIER 3":
                                    return select(all_services,tier_3)
                                    case "TIER 4":
                                    return select(all_services,tier_4)
                                    case "ALL SERVICES":
                                    return select(all_services,all_services)
                                    case "CUSTOM":
                                    return all_services
                                }
                                '''
                            ]
                    ]
            ],
    ]),

    pipelineTriggers([
        parameterizedCron('''
# Restart services at 12:30 everyday
30 12 * * * %ENVIRONMENT=PROD - DC2;strategy=CUSTOM;maxSurge=10%;maxUnavailable=0%;selected_services=...
        ''')
    ])
])