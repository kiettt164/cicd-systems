def ANSIBLE_INVENTORY = 'inventory/my-forlio/hosts'
def ANSIBLE_PLAYBOOK = 'playbooks/my-forlio'


properties([
    parameters([
        choice(
            choices: [
                'app-server-01',
                'app-server-02',
                'all_servers',
            ],
            name: 'HOSTNAME'
        ),

        [
            $class: 'ChoiceParameter',
            choiceType: 'PT_SINGLE_SELECT',
            name: 'SELECT_PLAYBOOK',
            script: [
                $class: 'GroovyScript',
                script: [
                    classpath: [],
                    sandbox: true,
                    script:
                        'return ["release-container","deploy-container","create_images","push_images","stop-container"]'
                ]
            ]
        ]
    ])
])

pipeline {
    agent any

    stages {
        stage('Checkout SCM') {
            steps {
                checkout scm
            }
        }

        stage('APPROVAL') {
            steps{
                timeout(time: 10, unit: 'MINUTES'){
                input(
                    message: "Should we continue?",
                    )
                }
            }
            
        }

        stage('Execute Playbook') {
            steps {
                script {
                    def playbookName = params.SELECT_PLAYBOOK
                    echo "Selected playbook: ${playbookName}"
                    withCredentials([string(credentialsId: 'ansible-vault-password', variable: 'VAULT_PASSWORD')]) {
                            sh """
                            whoami
                            pwd 
                            sudo -u jenkins ansible-playbook $ANSIBLE_PLAYBOOK/${playbookName}.yaml -i $ANSIBLE_INVENTORY --extra-vars "hostname=$HOSTNAME"
                            """
                    }
                }
            }
        }
    }
}