#AWS Region
variable "aws_region" {
  description = "Region in which AWS Resource"
  type = string
  default = "ap-southeast-1"
}

#AWS EC2 Instance type
variable "instance_type" {
  description = "EC2 Instance Type"
  type = string
  default = "t2.micro"
}

#Key Pair
variable "instance_keypair" {
  description = "Key Pair"
  type = string
  default = "tf-key"
}

