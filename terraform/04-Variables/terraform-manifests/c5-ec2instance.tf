# Resource: EC2 Instance
resource "aws_instance" "myec2vm" {
  ami = data.aws_ami.amzlinux2
  instance_type = variable.instance_type
  key_name = variable.instance_keypair
  vpc_security_group_ids = [aws_security_group.vpc-ssh, aws_security_group.vpc-web]
  tags = {
    "Name" = "EC2 Demo"
  }
}